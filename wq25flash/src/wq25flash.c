#include "hal.h"
#include "wq25flash.h"

void SPI2_Send (uint8_t *dt, uint16_t cnt)
{
  spiSend(&SPID2, cnt, dt);
}

void SPI2_Recv (uint8_t *dt, uint16_t cnt)
{
  spiReceive(&SPID2, cnt, dt);
}

void W25_Reset (void)
{
  cs_reset();
  tx_buf[0] = reset1;
  tx_buf[1] = reset2;
  SPI2_Send(tx_buf, 2);
  cs_set();
}


void WriteEnable_flash(void)
{
    cs_reset();
    tx_buf[0] = WriteEnable;
    SPI2_Send(tx_buf,1);
    cs_set();
}

void W25_Read_Data(uint32_t addr, uint8_t* data, uint32_t sz)
{
  cs_reset();
  tx_buf[0] = ReadData;
  tx_buf[1] = (addr >> 16) & 0xFF;
  tx_buf[2] = (addr >> 8) & 0xFF;
  tx_buf[3] = addr & 0xFF;
  SPI2_Send(tx_buf, 4);
  SPI2_Recv(data, sz);
  cs_set();
}

void W25_Write_Data(uint32_t addr, uint8_t* data, uint32_t sz)
{
  WriteEnable_flash();
  cs_reset();
  tx_buf[0] = WriteData;
  tx_buf[1] = (addr >> 16) & 0xFF;
  tx_buf[2] = (addr >> 8) & 0xFF;
  tx_buf[3] = addr & 0xFF;
  SPI2_Send(tx_buf, 4);
  SPI2_Send(data, sz);
  cs_set();
}
uint32_t W25_Read_ID(void)
{
  uint8_t dt[4];
  tx_buf[0] = JEDECID;
  cs_reset();
  SPI2_Send(tx_buf, 1);
  SPI2_Recv(dt,3);
  cs_set();
  return (dt[0] << 16) | (dt[1] << 8) | dt[2];
}
void W25_Ini(void)
{
  chThdSleepMilliseconds(100);
  W25_Reset();
  chThdSleepMilliseconds(100);
 // unsigned int id = W25_Read_ID();


}
void erase_sector4KB(uint32_t addr)
{
    WriteEnable_flash();
    cs_reset();
    tx_buf[0] = SectErase4KB;
    tx_buf[1] = (addr >> 16) & 0xFF;
    tx_buf[2] = (addr >> 8) & 0xFF;
    tx_buf[3] = addr & 0xFF;
    SPI2_Send(tx_buf,4);
    cs_set();
}
void erase_sector32KB(uint32_t addr)
{
    WriteEnable_flash();
    cs_reset();
    tx_buf[0] = SectErase32KB;
    tx_buf[1] = (addr >> 16) & 0xFF;
    tx_buf[2] = (addr >> 8) & 0xFF;
    tx_buf[3] = addr & 0xFF;
    SPI2_Send(tx_buf,4);
    cs_set();
}
void erase_sector64KB(uint32_t addr)
{
    WriteEnable_flash();
    cs_reset();
    tx_buf[0] = SectErase64KB;
    tx_buf[1] = (addr >> 16) & 0xFF;
    tx_buf[2] = (addr >> 8) & 0xFF;
    tx_buf[3] = addr & 0xFF;
    SPI2_Send(tx_buf,4);
    cs_set();
}
void chip_erase(void)
{
    WriteEnable_flash();
    cs_reset();
    tx_buf[0] = chiperase;
    SPI2_Send(tx_buf,1);
    cs_set();
}

void Uinque_ID(uint8_t uinque[])
{
    cs_reset();
    tx_buf[0] = UinqueID;

}

void WriteSR(uint8_t SR_address, uint8_t SR_data)
{
    WriteEnable_flash();
    cs_reset();
    tx_buf[0] = SR_address;
    tx_buf[1] = SR_data;
    SPI2_Send(tx_buf,2);
    cs_set();

}
uint8_t ReadSR(uint8_t SR_address)
{
    uint8_t RSR[1] = {0};
    cs_reset();
    tx_buf[0] =  SR_address;
    SPI2_Send(tx_buf,1);
    SPI2_Recv(RSR,1);
    cs_set();

    return RSR[0];
}
